<?php

namespace App\Casters;

use App\Collections\CollectionOfTag;
use App\DTO\TagDto;
use Illuminate\Support\Collection;
use Spatie\DataTransferObject\Caster;

class TagCollectionCaster implements Caster
{
    
    public function cast(mixed $value): CollectionOfTag
    {
        return new CollectionOfTag(array_map(function(array $data){
            return new TagDto(...$data);
        }, $value->toArray()));
    }
}
?>