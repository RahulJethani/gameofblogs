<?php

namespace App\DTO;

use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class UserDto extends DataTransferObject{
    
    public int $id;
    public string $name;
    public string $email;
    public string $role;
    public ?string $about;
}

?>