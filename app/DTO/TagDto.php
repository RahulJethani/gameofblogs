<?php

namespace App\DTO;

use App\Tag;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Spatie\DataTransferObject\DataTransferObject;

class TagDto extends DataTransferObject{
    
    public int $id;
    public string $name;
    public ?Collection $postDtoCollection;



    public function getPostsCount(){
        return Tag::findOrFail($this->id)->posts()->count();
    }
}

?>