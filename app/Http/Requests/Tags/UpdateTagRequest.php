<?php

namespace App\Http\Requests\Tags;

use App\Tag;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(Tag::getUpdateValidationRules(), ['name' => 'unique:tags,name,'.$this->tag->id]);
    }
}
