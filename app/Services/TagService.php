<?php


namespace App\Services;

use App\Casters\TagCollectionCaster;
use App\Collections\CollectionOfTag;
use App\DTO\TagDto;
use App\Tag;

class TagService
{
    public function create(TagDto $tagDto)
    {
        Tag::persistTag($tagDto);
    }

    public function update(TagDto $tagDto, Tag $tag)
    {
        $tag->updateTag($tagDto);
    }

    public function getTags(): CollectionOfTag{
        $tags = Tag::all();
        $tagDtoCollection = (new TagCollectionCaster())->cast($tags);
        return $tagDtoCollection;
    }
}
