<?php


namespace App\Services;

use App\Casters\PostCollectionCaster;
use App\Casters\TagCollectionCaster;
use App\Category;
use App\Collections\CollectionOfPost;
use App\Collections\CollectionOfTag;
use App\DTO\CategoryDto;
use App\DTO\PostDto;
use App\DTO\UserDto;
use App\Post;
use App\Tag;
use App\User;

class PostService
{
    public function create(PostDto $postDto)
    {
        Post::persistPost($postDto);
    }

    public function update(PostDto $postDto, Post $post)
    {
        $post->updatePost($postDto, $post);
    }

    public function getPosts(): CollectionOfPost
    {
        if(!auth()->user()->isAdmin()){
            $posts = Post::withoutTrashed()->where('user_id', auth()->id())->with(['category', 'tags', 'author']);
        }else{
            $posts = Post::with(['category', 'tags', 'author'])->get();
        }

        $postDtoCollection = (new PostCollectionCaster())->cast($posts);
        

        return $postDtoCollection;
    }

    public function getTagDtoCollection($tagIds): CollectionOfTag{
        $tags = collect();
        foreach($tagIds as $id){
            $tags->push(Tag::findOrFail($id));
        }
        return (new TagCollectionCaster())->cast($tags);
    }

    public function getCategoryDto($id): CategoryDto{
        $category = Category::findOrFail($id);
        return new CategoryDto(...$category->toArray());
    }

    public function getUserDto($id): UserDto{
        $user = User::findOrFail($id);
        return new UserDto(...$user->toArray());
    }

}
