<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'rahul@example.com')->get()->first();
        if(!$user){
            User::create([
                'name'=>'Rahul Jethani',
                'email'=>'rahul@example.com',
                'password'=>Hash::make('12345678'),
                'role'=>'admin'
            ]);
        }else{
            $user->update(['role'=>'admin']);
        }

        User::create([
            'name'=>'Jon Snow',
            'email'=>'jon@example.com',
            'password'=>Hash::make('abcd1234')
        ]);

        User::create([
            'name'=>'Arya Stark',
            'email'=>'arys@example.com',
            'password'=>Hash::make('abcd1234')
        ]);

    }
}
