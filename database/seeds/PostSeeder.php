<?php

use App\Category;
use App\Post;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name'=>'News']);
        $categoryDesign = Category::create(['name'=>'Design']);
        $categoryTechnology = Category::create(['name'=>'Technology']);
        $categoryEngineering = Category::create(['name'=>'Engineering']);

        $tagCustomers = Tag::create(['name'=>'customers']);
        $tagDesign = Tag::create(['name'=>'design']);
        $tagLaravel = Tag::create(['name'=>'laravel']);
        $tagCoding = Tag::create(['name'=>'coding']);

        $post1 = Post::create([
            'title'=>'We relocated our office to Home!',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/1.jpeg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'title'=>'How to become succesfull Java developer',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/2.jpeg',
            'category_id'=>$categoryEngineering->id,
            'user_id'=>3,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        $post3 = Post::create([
            'title'=>'Virat Kohli the legend',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/3.jpeg',
            'category_id'=>$categoryNews->id,
            'user_id'=>3,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);
        
        $post4 = Post::create([
            'title'=>'Android Development',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/4.jpeg',
            'category_id'=>$categoryTechnology->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagCoding->id, $tagCustomers->id]);
        $post2->tags()->attach([$tagCoding->id, $tagLaravel->id, $tagDesign->id]);
        $post3->tags()->attach([$tagLaravel->id]);
        $post4->tags()->attach([$tagCoding->id, $tagCustomers->id, $tagDesign->id, $tagLaravel->id]);
    }
}
